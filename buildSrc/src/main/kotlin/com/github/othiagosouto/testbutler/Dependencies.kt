
class Test {
    val jUnit = "junit:junit:4.13.1"
    val truth = "com.google.truth:truth:1.1"
}

class Core {
    val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.10"
}

object Deps {
    val core = Core()
    val testing = Test()
}

